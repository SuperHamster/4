#include <iostream>
#include <fstream>
#include <string> 
#include <iomanip>
#include <sstream>
#include <cmath>
using namespace std;

int main() 
{ 
	setlocale(LC_ALL, "Russian");
	string str, str1, str2, str3, str4;
	ifstream shifr("shifr.txt");
	if (!shifr.is_open()) 
		{
			cout << "Не удалось открыть файл"<<endl; 
			system ("pause"); 
			return -1;
		}
	ifstream text("text.txt");
	if (!text.is_open())   
		{
			cout << "Не удалось открыть файл"<<endl; 
			system ("pause"); 
			return -2;
		}
	ofstream itog("itog.txt");
	getline (text, str3, '\0');

	while (shifr.good())
		{
			shifr >> str2 /* что меняем */ >> str1 /* И на что */;
			int a=0, s=0, t=0;
			while (a!=-1)																		
				{
					a=-1;		
					a = str3.find(str2);      
					if (a!=-1) 
						{
							str3.erase (a, str2.length()); 
							str3.insert(a,str1);		
							str4.insert(t, str3, 0, a - str2.length() + str1.length() + 1); 
							s = a-str2.length() + str1.length() + 1;
							t+=s;
							str3.erase (0, s);
						}
					
				}
		}
	itog << str4 << endl;
	itog.close();
	cout << "Текст успешно зашифрован" << endl;
	system ("pause");
	return 0;
}